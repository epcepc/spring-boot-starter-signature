package com.jason.web.support.signature.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;

public class Base64Utils {
	
	private static Logger logger = LoggerFactory.getLogger(Base64Utils.class);
	
	/** */
	/**
	 * <p>
	 * </p>
	 * @param base64
	 * @return
	 * @throws Exception
	 */
	public static byte[] decode(String base64){
		byte[] bytes = null;
		try {
			bytes = (new BASE64Decoder()).decodeBuffer(base64);
			return  bytes;
		} catch (IOException e) {
			logger.error("public static byte[] decode(String base64) ,e={}",e);
		}
		return bytes;
	}
	
	/** */
	/**
	 * <p>
	 * </p>
	 * @param bytes
	 * @return
	 * @throws Exception
	 */
	public static String encode(byte[] bytes) throws Exception {
		return (new BASE64Encoder()).encodeBuffer(bytes);
	}
	
}