package com.jason.web.support.signature.configureradapter;

import com.jason.web.support.signature.interceptor.SignatureInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by junlee on 2017/10/27.
 */
public class signatureConfigurer extends WebMvcConfigurerAdapter {
	
	@Value("${spring.signature.excludepath}")
	private String excludepath;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new SignatureInterceptor()).addPathPatterns("/**").excludePathPatterns(excludepath);
		super.addInterceptors(registry);
	}
	
}